$(document).ready(function () {
  alertify.YoutubeDialog || alertify.dialog('YoutubeDialog', function () {
    var iframe;
    return {
      // dialog constructor function, this will be called when the user calls alertify.YoutubeDialog(videoId)
      main: function (videoId) {
        //set the videoId setting and return current instance for chaining.
        return this.set({
          'videoId': videoId
        });
      },
      // we only want to override two options (padding and overflow).
      setup: function () {
        return {
          options: {
            //disable both padding and overflow control.
            padding: !1,
            overflow: !1,
          }
        };
      },
      // This will be called once the DOM is ready and will never be invoked again.
      // Here we create the iframe to embed the video.
      build: function () {
        // create the iframe element
        iframe = document.createElement('iframe');
        iframe.frameBorder = "no";
        iframe.width = "100%";
        iframe.height = "100%";
        // add it to the dialog
        this.elements.content.appendChild(iframe);

        //give the dialog initial height (half the screen height).
        this.elements.body.style.minHeight = screen.height * .5 + 'px';
      },
      // dialog custom settings
      settings: {
        videoId: undefined
      },
      // listen and respond to changes in dialog settings.
      settingUpdated: function (key, oldValue, newValue) {
        switch (key) {
          case 'videoId':
            iframe.src = "https://www.youtube.com/embed/" + newValue;
            break;
        }
      },
      // listen to internal dialog events.
      hooks: {
        // triggered when the dialog is closed, this is seperate from user defined onclose
        onclose: function () {
          iframe.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        },
        // triggered when a dialog option gets update.
        // warning! this will not be triggered for settings updates.
        onupdate: function (option, oldValue, newValue) {
          switch (option) {
            case 'resizable':
              if (newValue) {
                this.elements.content.removeAttribute('style');
                iframe && iframe.removeAttribute('style');
              } else {
                this.elements.content.style.minHeight = 'inherit';
                iframe && (iframe.style.minHeight = 'inherit');
              }
              break;
          }
        }
      }
    };
  });
  // ----------------------------------------------- 邱寶郎
  $("#movie_BO_01").click(function () {
    //show the dialog
    alertify.YoutubeDialog('ZMAk6cqe7cc').set({
      frameless: true
    })
  });

  $("#movie_BO_02").click(function () {
    //show the dialog
    alertify.YoutubeDialog('Qe7n6dx7164').set({
      frameless: true
    })
  });

  $("#movie_BO_03").click(function () {
    //show the dialog
    alertify.YoutubeDialog('mCvOsR_6Rug').set({
      frameless: true
    })
  });

  $("#movie_BO_04").click(function () {
    //show the dialog
    alertify.YoutubeDialog('4BW7IjZk2cs').set({
      frameless: true
    })
  });

  $("#movie_BO_05").click(function () {
    //show the dialog
    alertify.YoutubeDialog('KJVOakVNpFM').set({
      frameless: true
    })
  });

  // --------------------------------------------張棋惠

  $("#movie_H_01").click(function () {
    //show the dialog
    alertify.YoutubeDialog('uRm2NhmrKrM').set({
      frameless: true
    })
  });

  $("#movie_H_02").click(function () {
    //show the dialog
    alertify.YoutubeDialog('iQbwZX-HTCs').set({
      frameless: true
    })
  });

  $("#movie_H_03").click(function () {
    //show the dialog
    alertify.YoutubeDialog('hEQd9fCHeg4').set({
      frameless: true
    })
  });

  $("#movie_H_04").click(function () {
    //show the dialog
    alertify.YoutubeDialog('uemTw5X-5bU').set({
      frameless: true
    })
  });

  $("#movie_H_05").click(function () {
    //show the dialog
    alertify.YoutubeDialog('0GezIFGBqbk').set({
      frameless: true
    })
  });


  // --------------------------------------------陳昆煌

  $("#movie_J_01").click(function () {
    //show the dialog
    alertify.YoutubeDialog('ijYpbaAh1Z0').set({
      frameless: true
    })
  });

  $("#movie_J_02").click(function () {
    //show the dialog
    alertify.YoutubeDialog('cNS17nfNo4M').set({
      frameless: true
    })
  });

  $("#movie_J_03").click(function () {
    //show the dialog
    alertify.YoutubeDialog('qG-dKSSU5mM').set({
      frameless: true
    })
  });

  $("#movie_J_04").click(function () {
    //show the dialog
    alertify.YoutubeDialog('sfHp-bs_eIM').set({
      frameless: true
    })
  });

  $("#movie_J_05").click(function () {
    //show the dialog
    alertify.YoutubeDialog('jFoJx1qg3ik').set({
      frameless: true
    })
  });


  // --------------------------------------------孫榮

  $("#movie_K_01").click(function () {
    //show the dialog
    alertify.YoutubeDialog('I_z0I8M_TCQ').set({
      frameless: true
    })
  });

  $("#movie_K_02").click(function () {
    //show the dialog
    alertify.YoutubeDialog('Oj8txtL4Zao').set({
      frameless: true
    })
  });

  $("#movie_K_03").click(function () {
    //show the dialog
    alertify.YoutubeDialog('x7AmrqXIcHQ').set({
      frameless: true
    })
  });

  $("#movie_K_04").click(function () {
    //show the dialog
    alertify.YoutubeDialog('frHtCwzpV3s').set({
      frameless: true
    })
  });

  $("#movie_K_05").click(function () {
    //show the dialog
    alertify.YoutubeDialog('cgjyu2-mDWg').set({
      frameless: true
    })
  });


  // --------------------------------------------張秋永

  $("#movie_U_01").click(function () {
    //show the dialog
    alertify.YoutubeDialog('IlGdLdpcXmo').set({
      frameless: true
    })
  });

  $("#movie_U_02").click(function () {
    //show the dialog
    alertify.YoutubeDialog('41IKum0C83s').set({
      frameless: true
    })
  });

  $("#movie_U_03").click(function () {
    //show the dialog
    alertify.YoutubeDialog('B6K7gSX4ke0').set({
      frameless: true
    })
  });

  $("#movie_U_04").click(function () {
    //show the dialog
    alertify.YoutubeDialog('c43hkr-1zbQ').set({
      frameless: true
    })
  });

  $("#movie_U_05").click(function () {
    //show the dialog
    alertify.YoutubeDialog('5F1JHNWPPd8').set({
      frameless: true
    })
  });

});