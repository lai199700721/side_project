$(function () {
  // hamburger icon 的切換
  $(".hamburger").on("click", function () {
    $(this).toggleClass("is-active");
    var display = $("#md-nav").css("display");
    if (display == "none") {
      $("#md-nav").css("display", "block");
      $(".md-fake1-ul").css("display", "none");
      $(".md-fake2-ul").css("display", "none");
      $(".md-fake3-ul").css("display", "none");
    } else {
      $("#md-nav").css("display", "none");
    }
  });

  $("#md-fake1").on("click", function () {
    var display = $(".md-fake1-ul").css("display");
    if (display == "none") {
      $(".md-fake1-ul").css("display", "block");
      $(".md-fake2-ul").css("display", "none");
      $(".md-fake3-ul").css("display", "none");
    } else {
      $(".md-fake1-ul").css("display", "none");
    }

  });
  $("#md-fake2").on("click", function () {
    var display = $(".md-fake2-ul").css("display");
    if (display == "none") {
      $(".md-fake1-ul").css("display", "none");
      $(".md-fake2-ul").css("display", "block");
      $(".md-fake3-ul").css("display", "none");
    } else {
      $(".md-fake2-ul").css("display", "none");
    }

  });
  $("#md-fake3").on("click", function () {
    var display = $(".md-fake3-ul").css("display");
    if (display == "none") {
      $(".md-fake1-ul").css("display", "none");
      $(".md-fake2-ul").css("display", "none");
      $(".md-fake3-ul").css("display", "block");
    } else {
      $(".md-fake3-ul").css("display", "none");
    }

  });

});