$(function () {
  // hamburger icon 的切換
  $(".hamburger").on("click", function () {
    $(this).toggleClass("is-active");
    var display = $("#md-nav").css("display");
    if (display == "none") {
      $("#md-nav").css("display", "block");
      $(".md-fake1-ul").css("display", "none");
      $(".md-fake2-ul").css("display", "none");
      $(".md-fake3-ul").css("display", "none");
    } else {
      $("#md-nav").css("display", "none");
    }
  });

  $("#md-fake1").on("click", function () {
    var display = $(".md-fake1-ul").css("display");
    if (display == "none") {
      $(".md-fake1-ul").css("display", "block");
      $(".md-fake2-ul").css("display", "none");
      $(".md-fake3-ul").css("display", "none");
    } else {
      $(".md-fake1-ul").css("display", "none");
    }

  });
  $("#md-fake2").on("click", function () {
    var display = $(".md-fake2-ul").css("display");
    if (display == "none") {
      $(".md-fake1-ul").css("display", "none");
      $(".md-fake2-ul").css("display", "block");
      $(".md-fake3-ul").css("display", "none");
    } else {
      $(".md-fake2-ul").css("display", "none");
    }

  });
  $("#md-fake3").on("click", function () {
    var display = $(".md-fake3-ul").css("display");
    if (display == "none") {
      $(".md-fake1-ul").css("display", "none");
      $(".md-fake2-ul").css("display", "none");
      $(".md-fake3-ul").css("display", "block");
    } else {
      $(".md-fake3-ul").css("display", "none");
    }

  });

});



// ________________________________________button

$(function () {
  // 偵聽a click
  $("#Button_UP").bind("click", function () {
    // 取得目標區塊的y座標
    var target_top = $("#area1").offset().top;
    // 取得body物件 這種寫法在各瀏覽器上最保險
    var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
    // 移動捲軸無動畫效果
    //$body.scrollTop(target_top);
    // 移動捲軸有動畫效果
    $body.animate({
      scrollTop: target_top
    }, 1000);
  })
  // $("#Button_DOWN").bind("click", function () {
  //   // 取得目標區塊的y座標
  //   var target_top = $("#area2").offset().top;
  //   // 取得body物件 這種寫法在各瀏覽器上最保險
  //   var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $(
  //     'html,body');
  //   // 移動捲軸無動畫效果
  //   //$body.scrollTop(target_top);
  //   // 移動捲軸有動畫效果
  //   $body.animate({
  //     scrollTop: target_top
  //   }, 1000);
  // })
})